//
//  ViewController.swift
//  list view
//
//  Created by Singh Amritpal on 2020-09-12.
//  Copyright © 2020 Singh Amritpal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var register: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        login.backgroundColor = UIColor.blue
        register.backgroundColor = UIColor.red
    }

    @IBAction func onClickLogin(_ sender: Any) {
        let vc = (self.storyboard?.instantiateViewController(identifier: "loginVC")) as! LoginViewController
        self.navigationController?.present(vc, animated: true)
    }
    
    
    @IBAction func onClickRegister(_ sender: Any) {
        
      //  let vc = (self.storyboard?.instantiateViewController(identifier: "RegisterVC")) as! RegisterViewController
     //   self.present(vc, animated: true)
    
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
        
        
    }
    

}

