//
//  RegisterViewController.swift
//  list view
//
//  Created by Singh Amritpal on 2020-09-12.
//  Copyright © 2020 Singh Amritpal. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table1: UITableView!
    
    @IBOutlet weak var table2: UITableView!
    
    var myArray = ["Element1", "Element2", "Element3", "Element4", "Element5", "Element6"]
    var myElements = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table1.delegate = self
        table1.dataSource = self

        table2.delegate = self
        table2.dataSource = self
    
    }

     
    @IBAction func addTo2(_ sender: Any) {
        
        guard let index =  self.table1.indexPathForSelectedRow ,
              let selectedcell = self.table1.cellForRow(at: index) as? MyTableViewCell
        
        else { return }
        myArray.remove(at: index.row)
        table1.reloadData()
        myElements.append(selectedcell.title.text ?? "nil")
        print(myElements)
        table2.reloadData()
    }
    
    
    
    @IBAction func addTo1(_ sender: Any) {
        
        guard let index =  self.table2.indexPathForSelectedRow ,
              let selectedcell = self.table2.cellForRow(at: index) as? MyTableViewCell
        else { return }
        myElements.remove(at: index.row)
        table2.reloadData()
        myArray.append(selectedcell.title2.text ?? "nil")
        table1.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == table2
        {
            return myElements.count
        }
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == table2
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! MyTableViewCell
            let element = myElements[indexPath.row]
            cell.title2.text = element
        return cell
        }
        else{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyTableViewCell
        cell.title.text = myArray[indexPath.row]
        
        return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == table2{
            table1.reloadData()
            
        }
        else if tableView == table1{
            table2.reloadData()
        }
    }
}
